# Java Calculator

This was a school assignment in which we learned how to create GUIs using Swing. The goal was to create a GUI calculator.

**PROJECT REQUIREMENTS**
*  Create a Swing application to create a calendar that provides the 4 arithmetic operations (addition, subtraction, multiplication, and division)
*  User interface must have a numeric display (at the top), a single column of operation buttons on the right hand side (the 4 operands and "="), and data entry buttons (numbers 0-9, a decimal point, and a backspace key)
*  Interaction is done using mouse clicks. Interaction via keyboard input was not required
*  **Ensure that user can enter negative numbers** *-> This task was not completed upon submission or upon first commit, dated June 27th 2019*
*  The display should be configured so that the end-user cannot edit the display contents
*  **Check for division by zero and create a message dialog with an error message when it occurs** *-> This task was not completed upon submission or upon first commit, dated June 27th 2019*
*  Only 2 listeners should be created: one for data entry buttons, and one for the operand buttons
*  Application must exit when the CLOSE icon is clicked
*  Javadoc comments must be included
*  The results of the calculations must be displayed with the proper number of decimal points, ranging from 1 to 16
*  The position of the components should not change if window is resized.
*  Use loops and arrays to create buttons instead of using repetitive code

**What I learned on this project**
- How to create Swing applications
- How complex something as theoretically simple as a GUI calculator can be. 
- Use of enums to track last operand used by user (a dirty way to prevent incorrect calculations but hey it works!)

**Plans for the future**
* [ ]  Correct missed requirement to allow input of negative numbers. 
* [ ]  Correct missed requirement to throw exception and error message when dividing by zero
* [ ]  Optimize last operand tracking to a more efficient method

**How to run**
Open the project in Eclipse or another Java IDE and run the "Calculator.java" file in ApplicationTestPkg