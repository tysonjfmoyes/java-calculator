package swingPkg;

/**
 * @author tjmoy
 *
 */
public enum LastOperand {
	ADD,
	SUBTRACT,
	DIVIDE,
	MULTIPLY,
	CALCULATE,
}
