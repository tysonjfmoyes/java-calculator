/*
 * File Name:	Launch.java
 * Author:		Tyson Moyes
 * Date:		April 22nd 2019
 * Purpose:		This contains all the code used to create the GUI, as well as the actionListeners for all the buttons.
*/
package swingPkg;

// import Swing and AWT
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * The launcher. Contains the JFrame construction.
 * @author Tyson Moyes
 * @since JDK_1.8_201
 */
public class Launch extends Calculate{
	// Swing variables
	private static JFrame frame;
	private JTextField numberDisplay = new JTextField(50);
	private JPanel numpad = new JPanel();
	private JPanel operands = new JPanel();
	private JButton nums[] = new JButton[12];
	private JButton operators[] = new JButton[5];
	private Font numberFont = new Font("Calibri", Font.PLAIN, 30);
	private Font operandFont = new Font("Calibri", Font.PLAIN, 24);
	private Double d = 0.0;
	
	// Custom variables, used for the buttonClicked() method
	private String curText;
	private String buttonClicked;
	
	/**
	 * The launch constructor. Creates the JFrame and JPanels, as well as all buttons and action listeners. Also sets layouts for numpad, operands, and panels
	 * @param title , the name of the application (in this case, Moyes, Tyson - ACA 5)
	 */
	public Launch(String title) {
		// Create a JFrame
		frame = new JFrame(title);
		frame.setSize(600,400);
		
		// Frame exits on close
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Set up the text field
		numberDisplay.setPreferredSize(new Dimension(600,50));
		numberDisplay.setFont(numberFont);
		numberDisplay.setHorizontalAlignment(SwingConstants.RIGHT);
		
		// Set up the panel sizes and colours (I know you said colours weren't required but it just looks so nice with colours!)
		numpad.setPreferredSize(new Dimension(500, 350));
		numpad.setBackground(Color.BLUE);

		operands.setPreferredSize(new Dimension(80,350));
		operands.setBackground(Color.YELLOW);

		// set up the grid layout for the numpad
		GridLayout numpadLayout = new GridLayout(4,3,5,2); // same with the padding in between buttons! So much nicer looking!
		numpad.setLayout(numpadLayout);
		
		// Add the numbers buttons
		for (int i = 0; i < 9; i++) { nums[i] = new JButton(String.valueOf(i+1)); }
		
		// Add the bottom row and its listeners
		nums[9] = new JButton(String.valueOf(0));
		nums[10] = new JButton(".");
		nums[11] = new JButton("<-");
		
		for (int i = 0; i < 12; i++) {
			nums[i].setFont(numberFont);
			// Create the action listener to watch for clicks on the buttons
			nums[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// Create variables to pass to beenClicked()
					curText = numberDisplay.getText();
					buttonClicked = ((JButton) e.getSource()).getText();
					
					// call beenClicked() and set numberDisplay's text to its returned value
					beenClicked(curText, buttonClicked);
				}
			});
			numpad.add(nums[i]);
		}
		
		// set the layout for the operator row
		GridLayout operandsLayout = new GridLayout(5,1,5,5);
		operands.setLayout(operandsLayout);

		// create the operands buttons
		for (int i = 0; i < 5; i++) {
			operators[i] = new JButton("");
			operators[i].setFont(operandFont);
			operators[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// Create variables to pass to beenClicked()
					curText = numberDisplay.getText();
					buttonClicked = ((JButton) e.getSource()).getText();
					
					beenClicked(curText, buttonClicked);
				}
			});
		}
		
		// set the text to the proper buttons
		operators[0].setText("+");
		operators[1].setText("-");
		operators[2].setText("*");
		operators[3].setText("/");
		operators[4].setText("=");
		
		// Add the operator buttons to the operands panel
		for (int i = 0; i < 5; i++) { operands.add(operators[i]); }
		
		// Add everything to the frame using BorderLayout
		frame.add(numberDisplay, BorderLayout.PAGE_START);
		frame.add(numpad, BorderLayout.LINE_START);
		frame.add(operands, BorderLayout.LINE_END);
		
		frame.setVisible(true);
	} // Launch() 
	
/******************************************************CUSTOM METHODS********************************************************************************/
	
	private void beenClicked(String curText, String buttonClicked) {
		String newText = "";
		
		// check if curText is not empty. If it is, add it to the new text String
		if(curText != "") {
			newText += curText;
		}

		// check if button pressed is numeric
		if (isNumeric(buttonClicked)) {
			// number is numeric, add number to text display using addNumber method
			addNumber(newText, buttonClicked);
		}
		
		// button pressed is not numeric, check other possibilities
		else {
			// check if . was pressed
			if (buttonClicked == ".") {
				newText += ".";
				numberDisplay.setText(newText);
			}
			
			// check if backspace was pressed
			if (buttonClicked == "<-") {
				try {
					// use substring to remove the last character in the string
					newText = newText.substring(0, newText.length() - 1);
					numberDisplay.setText(newText);
				}
				
				// no number available to delete, throws StringIndexOutOfBoundsException, send error message
				catch (StringIndexOutOfBoundsException siobe) { JOptionPane.showMessageDialog(frame, "ERROR: No number to delete", "FAILURE", JOptionPane.ERROR_MESSAGE); }
			}
			
			// check the operand buttons
			if (buttonClicked == "+") {
				try {
					// call add function and set display text to null
					add(curText);
					numberDisplay.setText("");
				}
				// No number in the display throws NumberFormatException. Catch it and throw a JOptionPane error message
				catch (NumberFormatException nfe) { JOptionPane.showMessageDialog(frame, "ERROR: No number to add", "FAILURE", JOptionPane.ERROR_MESSAGE); }
			}
			
			if (buttonClicked == "-") {
				try {
					// call subtract function and set display to null
					subtract(curText);
					numberDisplay.setText("");
				}
				// No number in the display throws NumberFormatException. Catch it and throw a JOptionPane error message
				catch (NumberFormatException nfe) { JOptionPane.showMessageDialog(frame, "ERROR: No number to subtract", "FAILURE", JOptionPane.ERROR_MESSAGE); }
			}
			
			if (buttonClicked == "*") {
				try {
					multiply(curText);
					numberDisplay.setText("");
				}
				// No number in the display throws NumberFormatException. Catch it and throw a JOptionPane error message
				catch (NumberFormatException nfe) { JOptionPane.showMessageDialog(frame, "ERROR: No number to multiply", "FAILURE", JOptionPane.ERROR_MESSAGE); }
			}
			
			if (buttonClicked == "/") {
				try {
					divide(curText);
					numberDisplay.setText("");
				}
				// No number in the display throws NumberFormatException. Catch it and throw a JOptionPane error message
				catch (NumberFormatException nfe) { JOptionPane.showMessageDialog(frame, "ERROR: No number to divide", "FAILURE", JOptionPane.ERROR_MESSAGE); }
			}
			
			if (buttonClicked == "=") {
				try {
					if(last == LastOperand.ADD) { add(curText); }
					else if (last == LastOperand.SUBTRACT) { subtract(curText); }
					else if (last == LastOperand.MULTIPLY) { multiply(curText); }
					else if (last == LastOperand.DIVIDE) { divide(curText); }
					numberDisplay.setText(sendResult());
				}
				
				// No number in the display throws NumberFormatException. Catch it and throw a JOptionPane error message
				catch (NumberFormatException nfe) { JOptionPane.showMessageDialog(frame, "ERROR: No number to calculate", "FAILURE", JOptionPane.ERROR_MESSAGE); }
			
			} // if(buttonClicked == "=")
		} // else not numeric
	} // beenClicked()
	
	private void addNumber(String curText, String buttonClicked) {
		// add the value of the button to the curText string, and set the display text to curText
		curText += buttonClicked;
		numberDisplay.setText(curText);
	}
	
	// check if a string is numeric (used to determine whether to call addNumber or not)
	private boolean isNumeric(String str) {
		// parseDouble will throw a NumberFormatException if the value is not a number. If it's not, return false. 
		try {
			d = Double.parseDouble(str);
		}
		catch (NumberFormatException nfe) {
			return false;
		}
		// value is an number, return true
		return true;
	}
}