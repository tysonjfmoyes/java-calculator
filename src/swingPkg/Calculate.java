/*
 * File Name:	Calculate.java
 * Author:		Tyson Moyes
 * Date:		April 23 2019
 * Purpose:		This class contains all of the arithmetic functions called by Launch. Uses the LastOperand enum to determine the last operand
 * 				(used for the "equals" button)
*/
package swingPkg;

/**
 * The Calculate class contains all the math operations called by the operands
 * @author Tyson Moyes
 * @since JDK_1.8_201
 */
public class Calculate {
	/**
	 * Add the LastOperand enum
	 */
	public static LastOperand last;
	private static double num;
	private static double ans;
	
	/**
	 * Adds the number in the number display to the "ans" total
	 * @param numToCompute - the number currently in the number display
	 */
	public static void add(String numToCompute) {
		/*
		 * if the equals sign was the last button hit (via LastOperand enum), set num to 0
		 * 	(ex:
		 * 		if person enters 33+33, then =, it'll output 66.
		 * 		Without this code, if user clicks the subtract button, it will subtract 66, which is not what I want.
		 * 	)
		 * This code is used in all arithmetic methods
		*/ 	
		if(last == LastOperand.CALCULATE) {
			num = 0;
		}
		else {
			num = Double.valueOf(numToCompute);
			ans += num;
		}
		// change the enum to mark last operand as ADD
		last = LastOperand.ADD;
	}
	
	/**
	 * Subtracts the number in the number display to the "ans" total
	 * @param numToCompute - the number currently in the number display
	 */
	public static void subtract(String numToCompute) {
		if(last == LastOperand.CALCULATE) {
			num = 0;
		}
		else {
			num = Double.valueOf(numToCompute);
			ans -= num;
		}
		// change the enum to mark last operand as SUBTRACT
		last = LastOperand.SUBTRACT;
	}
	
	/**
	 * Multiplies the "ans" total by the number in the number display
	 * @param numToCompute - the number currently in the number display
	 */
	public static void multiply(String numToCompute) {
		if (last == LastOperand.CALCULATE) {
			num = 0;
		}
		else {
			num = Double.valueOf(numToCompute);
			ans *= num;
		}
		// change the enum to mark last operand as MULTIPLY
		last = LastOperand.MULTIPLY;
	}
	
	/**
	 * Divides the "ans" total by the number in the number display
	 * @param numToCompute - the number currently in the number display
	 */
	public static void divide(String numToCompute) {
		if (last == LastOperand.CALCULATE) {
			num = 0;
		}
		else {
			num = Double.valueOf(numToCompute);
			ans /= num;
		}
		// change the enum to mark the last operand as DIVIDE
		last = LastOperand.DIVIDE;
	}
	
	/**
	 * Called by the equals sign button, returns the "ans" variable as a string
	 * @return - ans as a String
	 */
	public static String sendResult() {
		last = LastOperand.CALCULATE;
		num = 0;
		return Double.toString(ans);
	}
}