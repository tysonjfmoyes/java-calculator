/*
 * File Name:	Calculator.java
 * Author:		Tyson Moyes
 * Date:		April 23 2019
 * Purpose:		This file uses invokeLater to create a new Runnable that calls the Launch method in Launch.java
*/
package applicationTestPkg;
import javax.swing.SwingUtilities;

import swingPkg.*;

/**
 * The main calculator application
 * @author Tyson Moyes
 * @since JDK_1.8_201
 */
public class Calculator {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Launch("Moyes, Tyson - ACA 5");
			} // run()
		}); // invokeLater()
	} // main()
} // Class
